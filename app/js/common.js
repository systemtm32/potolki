$(function() {

    var menu = $(".menu");
    $('.menuToggle').on('click', function() {
       $(menu).slideToggle(300, function(){
            if( $(this).css('display') === "none"){
                $(this).removeAttr('style');
            }
       });

    });

    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && ($(window).width() > 992)){
    $(window).scroll(function(){
            var $sections = $('section');
            $sections.each(function(i,el){
            var top  = $(el).offset().top-100;
            var bottom = top +$(el).height();
            var scroll = $(window).scrollTop();
            var id = $(el).attr('id');
            if( scroll > top && scroll < bottom){
                $('a.accent-color').removeClass('accent-color');
                $('a[href="#'+id+'"]').addClass('accent-color');

            }
            })

            if ( $(this).scrollTop() > 100 && menu.hasClass("default") ){
                menu.detach().prependTo( $('#header') );
                menu.fadeOut('fast',function(){
                    $(this).removeClass("default")
                           .addClass("fixed")
                           .fadeIn('fast');

                });
            } else if($(this).scrollTop() <= 100 && menu.hasClass("fixed")) {
                menu.detach().prependTo( $('#menu') );
                menu.fadeOut('fast',function(){
                    $(this).removeClass("fixed")
                           .addClass("default")
                           .fadeIn('fast');
                });
            }
        });  
}
	$('.slide-one').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
    $('.slide-two').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        1000:{
            items:3
        }
    }
});
 


});
